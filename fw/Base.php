<?php
namespace fw;
class Base {
	public static $isBuild = true;
	public static $usePartial = true;
	public static $prevTemplate = array();
	public static $nextTemplate = array();
	public static $data = array();

	public function Build() {
		if (static::$isBuild) {
			extract(static::$data);
			if (static::$usePartial) {
				foreach (static::$prevTemplate as $prev) {
					include($prev);
				}
			}
			if (!empty(static::$template)) {
				include(static::$template);
			}
			if (static::$usePartial) {
				foreach (static::$prevTemplate as $prev) {
					include($prev);
				}
			}
		}
	} //Build

	public function setTemplate() {
	}

	public function data($name,$value) {
		static::$data[$name] = $value;
	}
	public function nsname($ns) {
		$last_name = end(explode("\\",$ns));
		return $last_name;
	}
}