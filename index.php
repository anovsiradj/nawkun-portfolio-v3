<?php
require('fw/autoloader.php');
spl_autoload_register('avzloader');

$route = empty($_GET['routerequest']) ? array('main') : array_filter(explode("/", $_GET['routerequest']));

$base = new fw\Base;
$base->route = $route; //default: null
$base->nope = "app/template/nope.php"; //default: null

$base::$prevTemplate[] = "app/template/partial/top.php";
$base::$prevTemplate[] = "app/template/partial/navigate.php";
$base::$nextTemplate[] = "app/template/partial/bottom.php";

$appallow = array('main','archive');
$type = 'app';
$role = 'guest';
$name = $route[0];
$appspace = "{$type}\\{$role}\\{$name}";

if (in_array($base->route[0],$appallow)) {
	$app = new $appspace;
} else {
	$app = new app\nope;
}

$app->Build();